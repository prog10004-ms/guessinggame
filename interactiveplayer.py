"""
Represents an interactive player that plays the guessing game. The interactive player is represented by 
the corresponding class, InteractivePlayer which specializes the generic Player.

@Author: Prof. Magdin Stoica
@Course: Programming Priciples, PROG10004
"""
from player import Player
import guessinggame as game #NOTE: import rather than from ... import must be used due to circular import reference 

class InteractivePlayer(Player): #InteractivePlayer IS-A Player
    def __init__(self, name):
        """The __init__ method of the derived (specialized) class MUST call the __init__ method of the base class
        in order for the object to be correctly initialized with base attributes as well as the specific ones defined
        by this class"""

        #ensure the base class is initialized
        Player.__init__(self, name)

    def play(self):
        """Override the play() method in order to change its implementation and ask the guess from the user"""
        #ask the user for a number between 0 and 10
        self._guess = int(input(f'{self._name}: Please enter a number between {game.GuessingGame.getMinGuess()} and {game.GuessingGame.getMaxGuess()}: '))
        
