"""This module identifies the user-defined exceptions used in the program"""

class OperationCancelled(Exception):
    pass