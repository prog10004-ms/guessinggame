"""
Represents a player that plays the guessing game. The player has a name and responsible
for guessing in each round of the game and remembering their guess.

@Author: Prof. Magdin Stoica
@Course: Programming Priciples, PROG10004
"""
import random
import guessinggame as game #NOTE: import rather than from ... import must be used due to circular import reference 

class Player:
    def __init__(self, name):
        self._name = name
        self._guess = -1
    
    def getName(self):
        return self._name

    def getGuess(self):
        return self._guess

    def play(self):
        #determine a random number between 0 and 10 as the player's guess
        self._guess = random.randint(game.GuessingGame.getMinGuess(), game.GuessingGame.getMaxGuess())
        print(f'{self._name}: I guess {self._guess}')