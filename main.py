"""
Main module for a program that has the following requirements:

Software Requirements
Create an application where 3 players can play a guessing game. 
The application shall randomly choose a number between 0 and 9 and the 3 players, 
which are automatically created by the game, shall randomly guess a number between
0 and 10. The first player to guess correctly wins. Each player shall be identified by 
their names: Larry, Curly and Moe.” The program shall show the guess of each player, 
who won and in how many rounds.

Object-Oriented Analysis
Nouns: application, player(s), game, number, 0, 9, 3, name, Larry, Curly, Moe, program,
guess, round(s)

Object-Oriented Design
Classes: Application, GuessingGame, Player, int, string

@Author: Prof. Magdin Stoica
@Course: Programming Priciples, PROG10004
"""

from application import Application

try:
    #create the application object...
    app = Application()

    #... and ask it to run
    app.run()
except Exception as ex:
    print(f'An unexpected error has occurred. Please contact your adminstrator.\nError Message: {ex}')
