"""
Represents the guessing game logic itself of establishing a number to be guessed and 
coordinating the players who take turns guessing.

@Author: Prof. Magdin Stoica
@Course: Programming Priciples, PROG10004
"""
from player import Player
from interactiveplayer import InteractivePlayer
import random

class GuessingGame:
    #define static variables to define the range of guesses
    s_minGuess = 0
    s_maxGuess = 10

    def __init__(self):
        self._answer = -1
        self._roundCount = 0
        
        #create the players 
        self._playerList = []
        self._playerList.append(Player('Larry'))
        self._playerList.append(Player('Curly'))
        self._playerList.append(InteractivePlayer('Moe'))


    @staticmethod
    def getMinGuess():
        """Accessor static method for the static field variable s_minGuess"""
        return GuessingGame.s_minGuess

    #define a static accessor method for the max guess
    @staticmethod
    def getMaxGuess():
        """Accessor static method for the static field variable s_maxGuess"""
        return GuessingGame.s_maxGuess

    #define a static method to set the range of guesses called setGuessRange
    @staticmethod
    def setGuessRange(minGuess, maxGuess):
        """Static method that establishes the guess range that all games and players must abide by"""
        GuessingGame.s_minGuess = minGuess
        GuessingGame.s_maxGuess = maxGuess
    
    def start(self):
        """
        Starts and completes the guessing game by asking players to guess a number until one
        player wins.
        """    
        #determine the number to be guessed as the game answer
        self._answer = random.randint(GuessingGame.s_minGuess, GuessingGame.s_maxGuess) 

        #repeat game rounds while no winner is found
        winnerFound = False
        while not winnerFound:
            #start a new round
            self._roundCount += 1 

            #ask each player for their guess
            for iPlayer in range(len(self._playerList)):
                self._playerList[iPlayer].play()

            #TODO: tabulate the round by printing the guesses of each player. To separate
            #concerns define a new method to do so and call it here

            #check to see if any of the player guessed correctly and won the game
            winner = self.determineWinner()       

            #print the winner if one was found or continue playing
            if winner != None:
                #a winner was found, they guessed correctly
                winnerFound = True

                #let the user know who won the game
                print(f'{winner.getName()} won the game in {self._roundCount} rounds')        

    def determineWinner(self):
        """Determines if any of the players have guessed correctly in any given rounde"""
        #TODO: While checking for a winner provide hints for each player whether their guess is
        #too low or two high

        #check ech player's guess to determine if they won
        for player in self._playerList:
            #check if the player's answer is correct
            if player.getGuess() == self._answer:
                #player has won the game
                return player

        #nobody guessed correctly
        return None