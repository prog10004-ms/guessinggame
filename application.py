"""
Represents the application that user is running and is responsible for the user interactivity
concern.

@Author: Prof. Magdin Stoica
@Course: Programming Priciples, PROG10004
"""
from guessinggame import GuessingGame
from exceptions import OperationCancelled
 
class Application:
    def run(self):
        """The main method of the application that is the starting point of the user interactivity"""
        
        #determine the range of guesses by asking the user
        userMinGuess, userMaxGuess = self.askForGuessRange()

        #and configure the guessing game
        GuessingGame.setGuessRange(userMinGuess, userMaxGuess)
        
        #TODO: allow the user to configure whether hints should be provided and implement hints

        #create the game object
        game = GuessingGame()

        #TODO: allow the uswer to enroll players in the game instead of the game 
        #always having the same three hard-coded players. The user must establish the name of each player
        #and their type (automatic or interactive)

        #ask the game to start
        game.start()

    def askForGuessRange(self):
        """Asks the user for the range of guesses that are possible in the game""" 
        
        #repeat asking the user for the range until they provide a correct range or they cancel
        while True:
            try: 
                #ask the user for the guess range 
                userMinGuessInput = input('Please enter the minimum possible guess. Press [ENTER] to exit: ')
                if len(userMinGuessInput) == 0:
                    raise OperationCancelled('Setting the minimum guess was cancelled by the user')

                userMaxGuessInput = input('Please enter the maximum possible guess. Press [ENTER] to exit: ')
                if len(userMaxGuessInput) == 0:
                    raise OperationCancelled('Setting the maximum guess was cancelled by the user')
                
                #transform the use input into teh type that is expected
                userMinGuess = int(userMinGuessInput)
                userMaxGuess = int(userMaxGuessInput)

                #return the valid range to the caller
                return (userMinGuess, userMaxGuess)
            except ValueError as err:
                print(f'You have entered an incorrect range. The following error occurred: \n"{err}".\nPlease try again.')
            except OperationCancelled as err:
                #print the error message to let the user know what has gone wrong
                print(f'{err}.\nThe default range will be used of guesses between 0 and 10')
                return (0, 10)